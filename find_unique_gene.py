import pandas as pd


def is_unique(cell_expr_counts):
    """check if a container only has one nonzero value"""
    occurrence_counter = 0
    for expression_count in cell_expr_counts:
        if expression_count != 0:
            occurrence_counter += 1
    return occurrence_counter == 1


def lookup_cell_name(dataframe, cell_index):
    """return the cell name corresponding to the cell index"""
    cell_names = list(dataframe.columns.values)[1:]
    return cell_names[cell_index]


def print_csv_header():
    print("Gene Name,Cell Name,Frequency")


def get_unique_genes(dataframe):
    """for every gene, check if any is unique"""
    print_csv_header()
    for row in dataframe.itertuples():
        gene_name = row[1]
        expression_counts = row[2:]
        if is_unique(expression_counts):
            for cell_index in range(len(expression_counts)):
                if expression_counts[cell_index] != 0:
                    print(
                        "{},{},{}".format(
                            gene_name,
                            lookup_cell_name(dataframe, cell_index),
                            expression_counts[cell_index],
                        )
                    )


data = pd.read_csv("cleaned_utz.csv")
data = data[data.columns[1:]] # remove index column
get_unique_genes(data)
